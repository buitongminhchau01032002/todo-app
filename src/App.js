import List from'./list.js';
import React, {useState} from 'react'

function App() {
    const [list, setList] = useState(["dfdsfs","mfds"]);
    const delItem =(id) => {
        let temp=[];
        for (let i = 0; i < list.length; i++) {
            if (i !== id)
                temp.push(list[i]);
        }
        setList(temp);
    }
    const [newItem, setNewItem] = useState("");
    
    const addN = (newItem) => {
        let temp = [...list, newItem];
        setList(temp);
    }
    return (
        <>
            <div>
                <input type="text" onChange={e => setNewItem(e.target.value)}/>
                <button onClick={() => addN(newItem)}>
                    Thêm
                </button>
                <List list={list} del={(index)=>delItem(index)}></List>
            </div>
        </>
    );
}

export default App;
