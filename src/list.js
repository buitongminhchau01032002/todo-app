const List = ({list, del}) => {
	let ls = list.map((value, index) => {
		return (
			<div key={index}>
				<span>
					{value}
				</span>
				<button onClick={() => del(index)}>Xoá</button>
			</div>
		)
	})
	return (
		<div>
			{ls}
		</div>
	)
}

export default List;